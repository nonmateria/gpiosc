# gpiosc 

Utility to receive OSC messages to control rasperry pi's GPIO pins. 
Usage:

```sh
gpiosc [port] [address]

```
address is optional and it will default on reacting to incoming message on `/gpio`. 

Received OSC messages should have two int argument: the first is the GPIO number to address (any number from 1 to 27 is valid) and the second is the value to set (any value greater than 0 will be treated as HIGH, the others as LOW). As soon as a message for a pin is received the first time, that pin mode is set to OUTPUT.

To quit the program you can use SIGINT by pressing CTRL+C or sending a SIGTERM with another console with `killall gpiosc`. 

## building 

you need `liblo` installed (included dev packages) in your system, then you build with make, for example on raspbian you could get everything you need like this

```sh
sudo apt-get install build-essential
sudo apt-get install liblo7 liblo-dev
git clone https://git.sr.ht/~npisanti/gpiosc
cd gpiosc 
make

```
then you will have an executable in `bin/gpiosc`. 

To build in debug mode, you should `make clean` first and then `make debug`, the output executable will have the same name.

## DISCLAIMER

This is directly writing at the BCM2835 memory address, and it's tested just by using all the pins as digital I/O, i'm not responsible of any damage this program could do to any device connected to the GPIO that needs to be treated differently, for example LCD displays.

## thanks to 
[This](https://www.cs.uaf.edu/2016/fall/cs301/lecture/11_09_raspberry_pi.html) lecture and some of [this](https://raspberrypi.stackexchange.com/a/39691) code.

## license

Nicola Pisanti, MIT License 2020