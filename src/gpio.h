/* BCM2835 specs 
 * https://www.raspberrypi.org/app/uploads/2012/02/BCM2835-ARM-Peripherals.pdf
 */

#ifndef GPIO_HEADER_H_DEFINED
#define GPIO_HEADER_H_DEFINED

#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

struct gpio_t {
	int fdgpio;
	unsigned int * address;
};

void gpio_close( struct gpio_t * gpio )
{
	if (gpio->fdgpio>=0) { 
		close( gpio->fdgpio );
	}
}

#ifdef __ARM_ARCH

int gpio_init( struct gpio_t * gpio )
{
	gpio->fdgpio = 0;
	gpio->address = NULL;
	
	gpio->fdgpio=open("/dev/gpiomem",O_RDWR);
	if (gpio->fdgpio<0) { 
		printf("Error opening /dev/gpiomem"); 
		return 1; 
	}
	
	gpio->address=(unsigned int *)mmap(0,4096,
		PROT_READ+PROT_WRITE, MAP_SHARED,
		gpio->fdgpio,0);

	return 0;
}

static inline void gpio_input( unsigned int * gpio_addr, unsigned int gpio_numb)
{
	*(gpio_addr + ((gpio_numb)/10)) &= ~(7u<<(((gpio_numb)%10)*3));	
}

static inline void gpio_output( unsigned int * gpio_addr, unsigned int gpio_numb)
{
	gpio_input( gpio_addr, gpio_numb);
	*(gpio_addr + ((gpio_numb)/10)) |=  (1u<<(((gpio_numb)%10)*3));

}

static inline void gpio_high( unsigned int * gpio_addr, unsigned int gpio_numb)
{
	*(gpio_addr + 7) = 1u << gpio_numb;
}

static inline void gpio_low( unsigned int * gpio_addr, unsigned int gpio_numb)
{
	*(gpio_addr + 10) = 1u << gpio_numb;
}

#else

int gpio_init( struct gpio_t * gpio )
{
	gpio->fdgpio = 0;
	gpio->address = NULL;
	
	printf( "[gpio] dummy GPIO initialization\n" );
	return 0;
}

static inline void gpio_input( unsigned int * gpio_addr, unsigned int gpio_numb)
{
	printf( "[gpio] set pin %d to input\n", gpio_numb );
    gpio_addr++;
}

static inline void gpio_output( unsigned int * gpio_addr, unsigned int gpio_numb)
{
	printf( "[gpio] set pin %d to output\n", gpio_numb );
    gpio_addr++;
}

static inline void gpio_high( unsigned int * gpio_addr, unsigned int gpio_numb)
{
	printf( "[gpio] set pin %d high\n", gpio_numb );
	gpio_addr++;
}

static inline void gpio_low( unsigned int * gpio_addr, unsigned int gpio_numb)
{
	printf( "[gpio] set pin %d low\n", gpio_numb );
	gpio_addr++;
}

#endif

#endif // GPIO_HEADER_H_DEFINED
