
#include "gpio.h"
#include <lo/lo.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

#define MAXPIN 27

void quit_handler(int signo);

void osc_error(int num, const char *m, const char *path);
int osc_handler(const char *path, const char *types, lo_arg **argv,
                int argc, void *data, void *user_data);

int args_are_not_valid(int argc, char *argv[]);

// --- globals -------
int running = 1;
struct gpio_t gpio;
unsigned int available[MAXPIN + 1];

int main(int argc, char *argv[])
{
	// --------- args -------------------------
	if (args_are_not_valid(argc, argv)) {
		return 1;
	}
	const char *port = argv[1];
	const char *default_address = "/gpio";
	const char *address = default_address;
	if (argc == 3)
		address = argv[2];

	// --------- signal handling -------------
	if (signal(SIGINT, quit_handler) == SIG_ERR) {
		printf("error on assigning signal handler\n");
		return 2;
	}
	if (signal(SIGTERM, quit_handler) == SIG_ERR) {
		printf("error on assigning signal handler\n");
		return 2;
	}

	// --------- gpio init --------------------
	int rc = gpio_init(&gpio);
	if (rc != 0) {
		printf("error mapping gpio memory\n");
		return 1;
	}

	for (int i = 0; i <= MAXPIN; ++i) {
		available[i] = 0;
	}

	// --------- osc server setup -------------
	lo_server receiver = lo_server_new(port, osc_error);
	lo_server_add_method(receiver, address, "ii", osc_handler, &gpio);

	// --------- main loop --------------------
	printf("waiting for messages on port %s...\n", port);
	while (running) {
		lo_server_recv_noblock(receiver, 1000);
	}

	// --------- cleaning ---------------------
	printf("cleaning ...\n");
	lo_server_free(receiver);
	gpio_close(&gpio);

	return 0;
}

static int check_pin(int gpio_numb)
{
	if (gpio_numb <= 0) {
		printf("[error] pin number must be greater than 0\n");
		return 0;
	} else if (gpio_numb >= MAXPIN) {
		printf("[error] pin number must be less or equal to %d\n", MAXPIN);
		return 0;
	}
	return 1;
}

int osc_handler(const char *path, const char *types, lo_arg **argv,
                int argc, void *data, void *user_data)
{
	int pin = argv[0]->i;
	int val = argv[1]->i;

	if (check_pin(pin)) {
		if (!available[pin]) {
			gpio_output(gpio.address, (unsigned int)pin);
			available[pin] = 1;
		}
		if (val > 0) {
			gpio_high(gpio.address, (unsigned int)pin);
		} else {
			gpio_low(gpio.address, (unsigned int)pin);
		}
	}

	return 0;
}

void osc_error(int num, const char *msg, const char *path)
{
	printf("liblo server error %d in path %s: %s\n", num, path, msg);
	fflush(stdout);
}

int args_are_not_valid(int argc, char *argv[])
{
	if (argc == 1 || argc > 3) {
		printf("USAGE: osc_gpio [port] [address]\n"
		       "       address is optional and defaults "
		       "to receiving on /gpio\n");
		return 1;
	}

	int i = 0;
	char c = argv[1][i];
	while (c != '\0') {
		if (c < '0' || c > '9') {
			printf("[error] input port argument should be a number\n");
			return 1;
		}
		i++;
		c = argv[1][i];
	}

	return 0;
}

void quit_handler(int signo)
{
	if (signo == SIGINT || signo == SIGTERM) {
		printf("\nreceived SIGINT or SIGTERM, quitting...\n");
		running = 0;
	}
}
